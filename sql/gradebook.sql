-- Create a lookup table of semesters/terms
DROP TABLE IF EXISTS term CASCADE;
CREATE TABLE IF NOT EXISTS term
(
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT UNIQUE NOT NULL
);

-- Insert entries for Fall, Spring and Summer terms to ensure consistency for developers
INSERT INTO term (id,name) VALUES (1,'Spring');
INSERT INTO term (id,name) VALUES (2,'Summer');
INSERT INTO term (id,name) VALUES (3,'Fall');

-- Create students table
DROP TABLE IF EXISTS student CASCADE;
CREATE TABLE IF NOT EXISTS student
(
  id SERIAL NOT NULL PRIMARY KEY,
  first_name TEXT NOT NULL,
  last_name TEXT NOT NULL,
  username VARCHAR(64) NOT NULL
);

-- Create course table
DROP TABLE IF EXISTS course CASCADE;
CREATE TABLE IF NOT EXISTS course
(
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description TEXT NOT NULL,
  section INTEGER NOT NULL DEFAULT 1,
  year INTEGER NOT NULL,
  term_id INTEGER NOT NULL REFERENCES term(id),
  UNIQUE (name,section,year,term_id)
);

-- Create course enrollment table
DROP TABLE IF EXISTS course_enrollment CASCADE;
CREATE TABLE IF NOT EXISTS course_enrollment
(
  student_id INTEGER NOT NULL REFERENCES student(id),
  course_id INTEGER NOT NULL REFERENCES course(id),
  UNIQUE (student_id,course_id)
);

-- Create course category table
DROP TABLE IF EXISTS category CASCADE;
CREATE TABLE IF NOT EXISTS category
(
  id SERIAL NOT NULL PRIMARY KEY,
  course_id INTEGER NOT NULL REFERENCES course(id),
  name TEXT NOT NULL,
  weight INTEGER NOT NULL,
  UNIQUE (course_id,name)
);

-- Create assignment table
DROP TABLE IF EXISTS assignment CASCADE;
CREATE TABLE IF NOT EXISTS assignment
(
  id SERIAL NOT NULL PRIMARY KEY,
  category_id INTEGER NOT NULL REFERENCES category(id),
  points DECIMAL NOT NULL,
  name TEXT NOT NULL,
  description TEXT
);

-- Create assignment grade table
DROP TABLE IF EXISTS assignment_grade CASCADE;
CREATE TABLE IF NOT EXISTS assignment_grade
(
  assignment_id INTEGER REFERENCES assignment(id),
  student_id INTEGER REFERENCES student(id),
  grade DECIMAL,
  UNIQUE(assignment_id,student_id)
);

-- *********
-- * VIEWS *
-- *********

-- Nothing here... yet.

-- ***************************
-- * PROCEDURES AND TRIGGERS *
-- ***************************

-- A procedure for determining the number of usernames with a given base string
-- For generating increment numbers in the event of duplicate entries
DROP FUNCTION IF EXISTS generated_username_occurs(base VARCHAR);
CREATE OR REPLACE FUNCTION generated_username_occurs(base VARCHAR(32))
  RETURNS INTEGER AS $$
    BEGIN
      RETURN (SELECT COUNT(username) FROM student WHERE username LIKE base||'%');
    END;
  $$ LANGUAGE PLPGSQL;

-- A procedure for automatically generating consistent usernames for students and faculty
DROP FUNCTION IF EXISTS create_username();
CREATE OR REPLACE FUNCTION create_username()
  RETURNS TRIGGER AS $$
    DECLARE gen_name VARCHAR(64); gen_inc INTEGER;
    BEGIN
      IF NEW.username IS NULL THEN
        -- Generate the name and a possible increment value
        gen_name := LOWER(
            CONCAT(
                left(REGEXP_REPLACE(NEW.first_name,'[^a-zA-Z\d\s:]','','g'),1),
                left(REGEXP_REPLACE(NEW.last_name,'[^a-zA-Z\d\s:]','','g'),31)
            )
        );
        gen_inc := generated_username_occurs(gen_name);
        gen_inc := gen_inc + 1;
          IF gen_inc = 1 THEN
            NEW.username := gen_name;
          ELSE
            NEW.username := CONCAT(gen_name,gen_inc);
          END IF;
      END IF;
      RETURN NEW;
    END;
  $$ LANGUAGE PLPGSQL;

-- A trigger to ensure that a username is present or otherwise generated on every insertion into the student table
DROP TRIGGER IF EXISTS trigger_generate_student_username ON student;
CREATE TRIGGER trigger_generate_student_username
  BEFORE INSERT ON student
  FOR EACH ROW
  EXECUTE PROCEDURE create_username();

-- Summarize a given student's grade in a given course
DROP FUNCTION IF EXISTS get_student_grade_summary(sid INTEGER, cid INTEGER);
CREATE OR REPLACE FUNCTION get_student_grade_summary(sid INTEGER, cid INTEGER)
  RETURNS TABLE (category TEXT,
                 weight INTEGER,
                 earned DECIMAL,
                 attempted DECIMAL,
                 total DECIMAL,
                 attempted_percentage DECIMAL,
                 total_percentage DECIMAL,
                 weighted_attempted_percentage DECIMAL,
                 weighted_total_percentage DECIMAL)
  AS
  $$
  BEGIN
    RETURN QUERY
    SELECT
      category.name AS category,
      category.weight AS weight,
      SUM(grade) AS earned,
      SUM(points) AS attempted,
      get_category_total(category.id) AS total,
      (SUM(grade)/SUM(points)) AS attempted_percentage,
      (SUM(grade)/get_category_total(category.id)) AS total_percentage,
      (SUM(grade)/SUM(points))*(category.weight/100.00) AS weighted_attempted_percentage,
      (SUM(grade)/get_category_total(category.id))*(category.weight/100.00) AS weighted_total_percentage
      FROM assignment_grade
      JOIN assignment ON assignment_id = assignment.id
      JOIN category ON category_id = category.id
      JOIN course ON course_id = course.id
      WHERE student_id = sid
      GROUP BY category.id
      HAVING course_id = cid;
  END;
  $$ LANGUAGE PLPGSQL;

-- Summarize a given student's grade in a given course
DROP FUNCTION IF EXISTS get_student_grade_detail(sid INTEGER, cid INTEGER);
CREATE OR REPLACE FUNCTION get_student_grade_detail(sid INTEGER, cid INTEGER)
  RETURNS TABLE (assignment TEXT,
                 grade DECIMAL,
                 points DECIMAL,
                 category TEXT,
                 weight INTEGER)
  AS
  $$
  BEGIN
    RETURN QUERY
    SELECT
      assignment.name AS assignment,
      assignment_grade.grade AS grade,
      assignment.points AS attempted,
      category.name AS category,
      category.weight AS weight
      FROM assignment_grade
      JOIN assignment ON assignment_id = assignment.id
      JOIN category ON category_id = category.id
      JOIN course ON course_id = course.id
      WHERE student_id = sid AND course_id = cid
      ORDER BY category.weight DESC, category.name DESC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Return a given student's current weighted grade in a given course
DROP FUNCTION IF EXISTS get_student_weighted_attempted_grade(sid INTEGER, cid INTEGER);
CREATE OR REPLACE FUNCTION get_student_weighted_attempted_grade(sid INTEGER, cid INTEGER)
  RETURNS TABLE (grade DECIMAL) AS
  $$
  BEGIN
    RETURN QUERY SELECT (SUM(weighted_attempted_percentage)*100) AS grade FROM get_student_grade_summary(sid,cid);
  END;
  $$ LANGUAGE PLPGSQL;

-- Get a list of all courses under the given name
DROP FUNCTION IF EXISTS get_course_list(cname TEXT);
CREATE OR REPLACE FUNCTION get_course_list(cname TEXT)
  RETURNS TABLE (id INTEGER, year INTEGER, term TEXT, section INTEGER) AS
  $$
  BEGIN
    RETURN QUERY
        SELECT course.id, course.year, term.name, course.section FROM course
        JOIN term ON course.term_id = term.id
        WHERE course.name = cname
        ORDER BY course.year DESC, course.term_id DESC, course.section ASC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Count course sections by year and term under the given name
DROP FUNCTION IF EXISTS get_course_sections(cname TEXT);
CREATE OR REPLACE FUNCTION get_course_sections(cname TEXT)
  RETURNS TABLE (year INTEGER, term TEXT, sections BIGINT) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT course.year, term.name, COUNT(section) FROM course
      JOIN term ON course.term_id = term.id
      WHERE course.name = cname
      GROUP BY course.year, term.name
      ORDER BY course.year DESC, term.name DESC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get all students enrolled in a given course
DROP FUNCTION IF EXISTS get_students(cid INTEGER);
CREATE OR REPLACE FUNCTION get_students(cid INTEGER)
  RETURNS TABLE (id INTEGER, last_name TEXT, first_name TEXT, username VARCHAR(64), current_grade DECIMAL) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT student.id, student.last_name, student.first_name, student.username, get_student_weighted_attempted_grade(student.id,cid) AS grade FROM student
      JOIN course_enrollment ON student.id = course_enrollment.student_id
      WHERE course_enrollment.course_id = cid
      ORDER BY student.last_name ASC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get categories for a given course
DROP FUNCTION IF EXISTS get_categories(cid INTEGER);
CREATE OR REPLACE FUNCTION get_categories(cid INTEGER)
  RETURNS TABLE (id INTEGER, name TEXT, weight INTEGER) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT category.id, category.name, category.weight FROM category
      WHERE category.course_id = cid
      ORDER BY id DESC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get category ID from a string
DROP FUNCTION IF EXISTS category_id(cid INTEGER, cname TEXT);
CREATE OR REPLACE FUNCTION category_id(cid INTEGER, cname TEXT)
  RETURNS TABLE (id INTEGER) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT category.id FROM category
      WHERE category.name = cname AND category.course_id = cid;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get student ID from a string
DROP FUNCTION IF EXISTS student_id(sname TEXT);
CREATE OR REPLACE FUNCTION student_id(sname TEXT)
  RETURNS TABLE (id INTEGER) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT student.id FROM student
      WHERE student.username = sname;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get assignment ID from a string
DROP FUNCTION IF EXISTS assignment_id(cid INTEGER, aname TEXT);
CREATE OR REPLACE FUNCTION assignment_id(cid INTEGER, aname TEXT)
  RETURNS TABLE (id INTEGER) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT assignment.id FROM assignment
      JOIN category ON assignment.category_id = category.id
      WHERE assignment.name = aname AND category.course_id = cid;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get all items for a given course, grouped by category
DROP FUNCTION IF EXISTS get_items(cid INTEGER);
CREATE OR REPLACE FUNCTION get_items(cid INTEGER)
  RETURNS TABLE (id INTEGER, category TEXT, name TEXT, description TEXT, points DECIMAL) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT assignment.id AS id, category.name AS category, assignment.name AS name, assignment.description AS description, assignment.points AS points FROM category
      JOIN assignment ON category.id = assignment.category_id
      WHERE category.course_id = cid
      ORDER BY id ASC;
  END;
  $$ LANGUAGE PLPGSQL;

-- Get point total for a given category
DROP FUNCTION IF EXISTS get_category_total(catid INTEGER);
CREATE OR REPLACE FUNCTION get_category_total(catid INTEGER)
  RETURNS TABLE (total_points DECIMAL) AS
  $$
  BEGIN
    RETURN QUERY
      SELECT SUM(assignment.points) AS total_points FROM assignment
      JOIN category ON assignment.category_id = category.id
      WHERE category_id = catid;
  END;
  $$ LANGUAGE PLPGSQL;