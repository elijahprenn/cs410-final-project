package src.edu.boisestate.cs410.gradebook;

import asg.cliche.Command;
import asg.cliche.ShellFactory;
import java.util.Properties;
import java.io.IOException;
import java.sql.*;

public class GradebookShell {

  private static enum Seasons { spring, summer, fall};
  
  static class Class {
    public Integer ID;
    public String term_id;
    public String section;
    public String year;
    public String course;
    Class(Integer ID, String year, String term_id, String section, String course) {
      this.ID = ID;
      this.term_id = term_id;
      this.section = section;
      this.year = year;
      this.course = course;
    }
    int getID() {
      return ID;
    }    
  };
  
  private Class activeClass = new Class(0, "No active class.", "", "", "");
  private final Connection db;

  public GradebookShell(Connection conn) { db = conn; }

  public boolean studentExists(int id) {
    Statement statement = null;
    try {
      String SQL = String.format("SELECT * FROM student WHERE id=%s;",id);
      statement = db.createStatement();
      ResultSet resultSet = statement.executeQuery(SQL);
      statement.close();
      return resultSet.next();
    } catch (SQLException e) {
      return false;
    } 
  }
  
  // /////////////////////////
  // Class management commands
  // /////////////////////////

  @Command(name="select-class", abbrev="sc")
  public String select_class(String course) {
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("SELECT sections FROM get_course_sections(?);");
      statement.setString(1,course);
      ResultSet rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: Found no sections!";
        return retVal;
      }
      int columnValue = rs.getInt(1);
      if(columnValue > 1) {
        retVal += "***ERROR: Ambigous course list!";
        return retVal;
      }
      statement = db.prepareStatement("SELECT * FROM get_course_list(?);");
      statement.setString(1, course);
      rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: No courses returned from call to get_course_list!";
        return retVal;
      }
      activeClass = new Class(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), course);
      retVal += "Selected class successfully!";
    } catch(SQLException e) {
      retVal += "***ERROR: Got SQL exception: " + e.toString(); 
    }
    return retVal;//active_class;
  }
  
  @Command(name="select-class", abbrev="sc")
  public String select_class(String course, String term, String year) {
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("SELECT sections FROM get_course_sections(?) WHERE year=(?) AND UPPER(term) LIKE UPPER(?);");
      statement.setString(1,course);
      statement.setInt(2,Integer.parseInt(year));
      statement.setString(3,term);
      ResultSet rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: Found no sections!";
        return retVal;
      }
      int columnValue = rs.getInt(1);
      if(columnValue > 1) {
        retVal += "***ERROR: Ambigous course list!";
        return retVal;
      }
      statement = db.prepareStatement("SELECT * FROM get_course_list(?) WHERE year=(?) AND UPPER(term) LIKE UPPER(?);");
      statement.setString(1, course);
      statement.setInt(2,Integer.parseInt(year));
      statement.setString(3,term);
      rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: No courses returned from call to get_course_list!";
        return retVal;
      }
      activeClass = new Class(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), course);
      retVal += "Selected class successfully!";
    } catch(SQLException e) {
      retVal += "***ERROR: Got SQL exception: " + e.toString(); 
    }
    return retVal;//active_class;
  }
  
  @Command(name="select-class", abbrev="sc")
  public String select_class(String course, String term, String year, int section) {
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("SELECT * FROM get_course_list(?) WHERE year=(?) AND UPPER(term) LIKE UPPER(?) AND section=(?);");
      statement.setString(1, course);
      statement.setInt(2,Integer.parseInt(year));
      statement.setString(3,term);
      statement.setInt(4, section);
      ResultSet rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: No courses returned from call to get_course_list!";
        return retVal;
      }
      activeClass = new Class(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), course);
      retVal += "Selected class successfully!";
    } catch(SQLException e) {
      retVal += "***ERROR: Got SQL exception: " + e.toString(); 
    }
    return retVal;//active_class;
  }

  @Command(name="new-class")
  public String new_class(String course, String term, int year, int section, String desc) {
    String retVal = "";
    try {
      int termID = 0;
      switch(term.toUpperCase()) {
        case "SPRING":
          termID = 1;
          break;
        case "FALL":
          termID = 3;
          break;
        case "SUMMER":
          termID = 2;
          break;
        default:
          retVal += "***ERROR: No matching term!";
          return retVal;
      }

      PreparedStatement statement = db.prepareStatement("INSERT INTO course (name,description,section,year,term_id) VALUES((?),(?),(?),(?),(?));");
      statement.setString(1, course);
      statement.setString(2, desc);
      statement.setInt(3, section);
      statement.setInt(4, year);
      statement.setInt(5, termID);
      int resultCode = statement.executeUpdate();
      if(resultCode == 1)
        retVal = "Was successfull!";
      else
        retVal = "***ERROR: FAILED!!";
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  @Command(name="show-class")
  public String show_class() {
    String retVal = "Active Class: " + activeClass.course + 
                    " " + activeClass.getID() +
                    " " + activeClass.term_id +
                    " " + activeClass.section +
                    " " + activeClass.year;
    return retVal;//active_class;
  }
  // /////////////////////////
  // Category and Item management commands
  // /////////////////////////

  @Command(name="show-categories")
  public String show_categories() {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("SELECT name,weight FROM get_categories(?);");
      statement.setInt(1, activeClass.getID());
      ResultSet rs = statement.executeQuery();
      ResultSetMetaData metaData = rs.getMetaData();
      int columns = metaData.getColumnCount();
      while(rs.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = rs.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
    } catch(SQLException e) {
      retVal += "***ERROR: Got SQL exception: " + e.toString(); 
    }
    return retVal;
  }

  @Command(name="add-category")
  public String add_category(String name, int weight) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("INSERT INTO category (course_id,name,weight) VALUES((?),(?),(?));");
      statement.setInt(1, activeClass.getID());
      statement.setString(2, name);
      statement.setInt(3, weight);
      int resultCode = statement.executeUpdate();
      if(resultCode == 1)
        retVal = "Was successfull!";
      else
        retVal = "***ERROR: FAILED!!";
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  @Command(name="show-items")
  public String show_items() {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    String retVal = "";
    try {
      PreparedStatement statement = db.prepareStatement("SELECT * FROM get_items(?);");
      statement.setInt(1, activeClass.getID());
      ResultSet rs = statement.executeQuery();
      ResultSetMetaData metaData = rs.getMetaData();
      int columns = metaData.getColumnCount();
      while(rs.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = rs.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
    } catch(SQLException e) {
      retVal += "***ERROR: Got SQL exception: " + e.toString(); 
    }
    return retVal;
  }
  
  @Command(name="add-item")
  public String add_item(String name, String category, String desc, int points) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    String retVal = "";
    int categoryID = 0;
    try {
        PreparedStatement statement = db.prepareStatement("SELECT * FROM category_id(?,?);");
        statement.setInt(1, activeClass.ID);
        statement.setString(2, category);
        ResultSet rs = statement.executeQuery();
        if(rs.next() == false) {
            retVal += "***ERROR: Found no categories!" + statement.toString();
            return retVal;
        }
        categoryID = rs.getInt(1);
        statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
      return retVal;
    }
    try {
      PreparedStatement statement = db.prepareStatement("INSERT INTO assignment (category_id,points,name,description) VALUES((?),(?),(?),(?));");
      statement.setInt(1, categoryID);
      statement.setInt(2, points);
      statement.setString(3, name);
      statement.setString(4, desc);
      int resultCode = statement.executeUpdate();
      if(resultCode == 1)
        retVal = "Was successful!";
      else
        retVal = "***ERROR: FAILED!!";
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }
  
  // /////////////////////////
  // Student management commands
  // /////////////////////////

  @Command(name="show-students")
  public String show_student() {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    Statement statement = null;
    String retVal = "";
    try {
      String SQL = String.format("SELECT * FROM student;");
      statement = db.createStatement();
      ResultSet resultSet = statement.executeQuery(SQL);
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columns = metaData.getColumnCount();
      while(resultSet.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = resultSet.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
      statement.close();
    } catch (SQLException e) {
      retVal = "**ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }
  
  @Command(name="show-students")
  public String show_student(String search) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    PreparedStatement statement = null;
    String retVal = "";
    try {
      String SQL = String.format("SELECT * FROM student WHERE first_name LIKE ? OR last_name LIKE ? OR username LIKE ?;");
      statement = db.prepareStatement(SQL);
      statement.setString(1, "%" + search + "%");
      statement.setString(2, "%" + search + "%");
      statement.setString(3, "%" + search + "%");
      ResultSet resultSet = statement.executeQuery();
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columns = metaData.getColumnCount();
      while(resultSet.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = resultSet.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
      statement.close();
    } catch (SQLException e) {
      retVal = "**ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }
  
  @Command(name="add-student")  
  public String add_student(String id, String lastName, String firstName) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    Statement statement = null;
    String retVal = "";
    try { 
      String SQL = "";
      if(!studentExists(Integer.parseInt(id)))
        SQL += String.format("INSERT INTO student (id,last_name,first_Name) VALUES(%s,'%s','%s');",id, lastName, firstName);
      SQL += String.format("INSERT INTO course_enrollment (student_id,course_id) VALUES(%s,%s);", activeClass.ID, id);
      statement = db.createStatement();
      int resultCode = statement.executeUpdate(SQL);
      if(resultCode == 1)
        retVal = SQL + " Was successfull!";
      else
        retVal = SQL + "***ERROR: FAILED!!";
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  @Command(name="grade")
  public String grade(String assignment, String username, int grade) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    PreparedStatement statement = null;
    String retVal = "";
    int assignmentID = 0;
    int studentID = 0;
    try {
        statement = db.prepareStatement("SELECT * FROM assignment_id(?,?);");
        statement.setInt(1, activeClass.ID);
        statement.setString(2, assignment);
        ResultSet rs = statement.executeQuery();
        if(rs.next() == false) {
            retVal += "***ERROR: Found no categories!" + statement.toString();
            return retVal;
        }
        assignmentID = rs.getInt(1);
        statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
      return retVal;
    }
    try {
        statement = db.prepareStatement("SELECT * FROM student_id(?);");
        statement.setString(1, username);
        ResultSet rs = statement.executeQuery();
        if(rs.next() == false) {
            retVal += "***ERROR: Found no categories!" + statement.toString();
            return retVal;
        }
        studentID = rs.getInt(1);
        statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
      return retVal;
    }
    try { 
      String SQL = "INSERT INTO assignment_grade (assignment_id, student_id, grade) VALUES((?),(?),(?));";
      statement = db.prepareStatement(SQL);
      statement.setInt(1, assignmentID);
      statement.setInt(2, studentID);
      statement.setInt(3, grade);
      int resultCode = statement.executeUpdate();
      if(resultCode == 1)
        retVal = SQL + " Was successfull!";
      else
        retVal = SQL + "***ERROR: FAILED!!";
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  // /////////////////////////
  // Grade reporting commands
  // /////////////////////////

  @Command(name="student-grades")
  public String student_grades(String username) {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    PreparedStatement statement;
    String retVal = "";
    int studentID;
    try {
      statement = db.prepareStatement("SELECT * FROM student_id(?);");
      statement.setString(1, username);
      ResultSet rs = statement.executeQuery();
      if(rs.next() == false) {
        retVal += "***ERROR: Found no student ID corresponding to supplied username!" + statement.toString();
        return retVal;
      }
      studentID = rs.getInt(1);
      statement.close();
    } catch (SQLException e) {
      retVal = "***ERROR: Got SQL exception: " + e.toString();
      return retVal;
    }
    try {
      String SQL = String.format("SELECT * FROM get_student_grade_detail(?,?);");
      statement = db.prepareStatement(SQL);
      statement.setInt(1,studentID);
      statement.setInt(2,activeClass.ID);
      ResultSet resultSet = statement.executeQuery();
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columns = metaData.getColumnCount();
      while(resultSet.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = resultSet.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
      statement.close();
    } catch (SQLException e) {
      retVal = "**ERROR: Got SQL exception: " + e.toString();
    }
    try {
      String SQL = String.format("SELECT * FROM get_student_grade_summary(?,?);");
      statement = db.prepareStatement(SQL);
      statement.setInt(1,studentID);
      statement.setInt(2,activeClass.ID);
      ResultSet resultSet = statement.executeQuery();
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columns = metaData.getColumnCount();
      while(resultSet.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = resultSet.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
      statement.close();
    } catch (SQLException e) {
      retVal = "**ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  @Command(name="gradebook")
  public String gradebook() {
    if(activeClass.ID == 0)
    {
      return "***ERROR: ACTIVE CLASS NOT SET!";
    }
    PreparedStatement statement = null;
    String retVal = "";
    try {
      String SQL = String.format("SELECT * FROM get_students(?);");
      statement = db.prepareStatement(SQL);
      statement.setInt(1,activeClass.ID);
      ResultSet resultSet = statement.executeQuery(SQL);
      ResultSetMetaData metaData = resultSet.getMetaData();
      int columns = metaData.getColumnCount();
      while(resultSet.next()) {
        for (int i = 1; i <= columns; i++) {
          if (i > 1)
            retVal += ",  ";
          String columnValue = resultSet.getString(i);
          retVal += columnValue.toString() + " " + metaData.getColumnName(i);
        }
        retVal += "\n";
      }
      statement.close();
    } catch (SQLException e) {
      retVal = "**ERROR: Got SQL exception: " + e.toString();
    }
    return retVal;
  }

  @Command(name="import-grades")
  public String import_grades() {
    return "***ERROR: Not implmented.";
  }
  
  public static void main(String[] args) throws IOException, SQLException {
    String dbUrl = args[0]; // labs.nfinit.systems:9500/gradebook
    Properties props = new Properties();
    props.setProperty("user","prenne");
    props.setProperty("password","wombat");
    try (Connection conn = DriverManager.getConnection("jdbc:postgresql://" + dbUrl, props)) {
      GradebookShell cliShell = new GradebookShell(conn);
      ShellFactory.createConsoleShell("->", "", cliShell).commandLoop();
    }
  }
}
